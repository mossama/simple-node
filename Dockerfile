FROM node:current-slim
# set maintainer
LABEL maintainer "Dell Summer Academy"

WORKDIR /usr/src/app
COPY package.json .
RUN npm install

# tell docker what port to expose
EXPOSE 8000

CMD [ "npm", "start" ]
# set a health check
HEALTHCHECK --interval=5s \
            --timeout=5s \
            CMD curl -f http://127.0.0.1:8000 || exit 1

COPY . .

CMD [ "npm", "start" ]