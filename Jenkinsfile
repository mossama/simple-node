pipeline {
  // The Agent which we will run our pipeline on it 
    agent any
    environment {
        secret = credentials('sonar1')
    }
  // Stage to Clone code 
  stages {
    stage('Clone repository') {
      steps {
        checkout scm
      }
    }
  // Stage Run Unit Test only on dev branch 
    stage('Running Unit Tests') {
      when {
        branch 'dev' 
      }
      steps {  
        sh 'echo "Running Unit tests only on Dev Branch "'
      }
    }
    // Stage to Build Docker Image 
    stage('Build image') {
      steps {  
        sh 'whoami'
        sh 'docker build . -t hello-node:$BUILD_NUMBER'
      }
    }
    // Stage to Test Code Coverage Running only on master branch 
    stage('Code Coverage') {
      when {
        branch 'master' 
      }
      steps {
        sh 'echo "Code Coverage Stage"'
      }
    }
    stage('Code Quality Check via SonarQube') {
      steps {
        script {
          def scannerHome = tool 'sonar_scanner';
        withSonarQubeEnv('sonar_server') {
          sh """
          ${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=test-node-js-test \
              -Dsonar.sources=. \
              -Dsonar.css.node=. \
              -Dsonar.host.url=http://40.117.95.161:9000 \
              -Dsonar.login=$secret
          """
        }
        }
       }
   }
    stage('Deploy Image') {
      steps {
          sh 'echo "Deploying container with build no. $BUILD_NUMBER"'
          sh 'docker rm --force node'
          sh 'docker run --name node -d -p 8000:8000 hello-node:$BUILD_NUMBER'             
      }
    }
    // Runnin parallel test Stage 
    stage('Running Integration and Functional Automation Tests') {
      steps {
        parallel(
          a: {
            echo "Running Integration Test"
          },
          b: {
            echo "Running Functional Autoamtion Tests"
          }
        )
      }
    }
  }
}
