// load the http module
var http = require('http');

// configure our HTTP server
var server = http.createServer(function (request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  const array = [1,5,2,4,3]
  const sorted = array.sort()
  response.end("Welcome to Dell 2021\n");
  //response.end(JSON.stringify(sorted, null,2))
});

// listen on localhost:8000
server.listen(8000);
console.log("Server listening at http://127.0.0.1:8000/");
